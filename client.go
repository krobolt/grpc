package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
	"os"
	"time"

	"grpc-test/users"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	address = "localhost:50051"
)

func main() {
	f, _ := os.Open("key/clientcert.pem")
	clientCertPEM, err := ioutil.ReadAll(f)
	if err != nil {
		panic(err)
	}
	f.Close()

	f, _ = os.Open("key/clientkey.pem")
	clientKeyPEM, err := ioutil.ReadAll(f)
	if err != nil {
		panic(err)
	}
	f.Close()

	f, _ = os.Open("key/rootcert.pem")
	rootCertPEM, err := ioutil.ReadAll(f)
	if err != nil {
		panic(err)
	}
	f.Close()

	certPool := x509.NewCertPool()
	certPool.AppendCertsFromPEM(rootCertPEM)

	clientTLSCert, err := tls.X509KeyPair(clientCertPEM, clientKeyPEM)

	TLSConfig := &tls.Config{
		ServerName:         "127.0.0.1",
		InsecureSkipVerify: false,
		RootCAs:            certPool,
		Certificates:       []tls.Certificate{clientTLSCert},
	}

	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(credentials.NewTLS(TLSConfig)))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	cuser := users.NewUsersClient(conn)

	// Contact the server and print out its response.

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	res, err := cuser.List(ctx, &users.UsersRequest{Category: "all"})
	log.Println(res, err)

}
