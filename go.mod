module grpc-test

go 1.17

require (
	github.com/golang/protobuf v1.5.2 // indirect
	gitlab.com/krobolt/go-dispatcher v0.0.0-20210814194629-468232dd7016
	gitlab.com/krobolt/go-router v0.0.0-20210821025418-50bca5448fcf
	golang.org/x/net v0.0.0-20211007125505-59d4e928ea9d // indirect
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20211007155348-82e027067bd4 // indirect
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)
