package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	pb "grpc-test/pb"
	"grpc-test/users"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	dispatcher "gitlab.com/krobolt/go-dispatcher"
	router "gitlab.com/krobolt/go-router"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	address     = "localhost:50051"
	defaultName = "world"
)

func selfsigned() credentials.TransportCredentials {
	f, _ := os.Open("key/clientcert.pem")
	clientCertPEM, err := ioutil.ReadAll(f)
	if err != nil {
		log.Println(err)
	}
	f.Close()

	f, _ = os.Open("key/clientkey.pem")
	clientKeyPEM, err := ioutil.ReadAll(f)
	if err != nil {
		log.Println(err)

	}
	f.Close()

	f, _ = os.Open("key/rootcert.pem")
	rootCertPEM, err := ioutil.ReadAll(f)
	if err != nil {
		log.Println(err)

	}
	f.Close()

	certPool := x509.NewCertPool()
	certPool.AppendCertsFromPEM(rootCertPEM)

	clientTLSCert, err := tls.X509KeyPair(clientCertPEM, clientKeyPEM)

	TLSConfig := &tls.Config{
		ServerName:         "127.0.0.1",
		InsecureSkipVerify: false,
		RootCAs:            certPool,
		Certificates:       []tls.Certificate{clientTLSCert},
	}

	//conn, err := grpc.Dial(address, grpc.WithTransportCredentials(credentials.NewTLS(TLSConfig)))
	return credentials.NewTLS(TLSConfig)

}

func dial(ctx context.Context) (*grpc.ClientConn, error) {
	return grpc.DialContext(
		ctx,
		address,
		grpc.WithInsecure(),
		//grpc.WithTimeout(time.Second*2),
		grpc.WithTransportCredentials(selfsigned()),
		grpc.FailOnNonTempDialError(true),
		grpc.WithBlock(),
	)
}

func handleGreet(w http.ResponseWriter, r *http.Request) {

	log.Println("handle")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	conn, err := dial(ctx)
	if err != nil {
		log.Println(err.Error())
		w.Write([]byte("api offline"))
		return
	}

	defer conn.Close()

	log.Println("handle sutnc")

	c := pb.NewSyncClient(conn)
	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	//call functions from server
	res, err := c.SayHello(ctx, &pb.HelloRequest{Name: defaultName})
	if err != nil {
		log.Println(err.Error())
		w.Write([]byte("api offline"))
	}
	log.Printf("Greeting: %s", res.GetMessage())
	w.Write([]byte(res.GetMessage()))

}

func handleListUsers(w http.ResponseWriter, r *http.Request) {

	action := r.URL.Query().Get("category")
	log.Println("handle action:", action)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	conn, err := dial(ctx)
	if err != nil {
		log.Println(err.Error())
		w.Write([]byte("api offline"))
		return
	}

	defer conn.Close()

	c := pb.NewSyncClient(conn)
	cuser := users.NewUsersClient(conn)

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	//call functions from server
	res, err := c.SayHello(ctx, &pb.HelloRequest{Name: defaultName})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Greeting: %s", res.GetMessage())

	//call more server functions
	resUser, err := cuser.List(ctx, &users.UsersRequest{Category: action})
	log.Println(resUser, err)

	names := resUser.GetId()

	out, err := json.Marshal(names)

	log.Println(err)
	w.Write(out)

}

func main() {

	Dispatch := dispatcher.NewDispatcher(
		"/",
		router.NewDefaultOptions(),
		[]string{"global"},
	)

	Dispatch.Group("/api/v1", func() {
		Dispatch.Add("GET", "/greet", handleGreet, nil)
		Dispatch.Add("GET", "/users/:(a-zA-Z0-9-_.)-category", handleListUsers, nil)
	}, nil)

	mux := http.NewServeMux()
	mux.Handle("/", Dispatch)

	h := &http.Server{
		Addr:              ":8181",
		Handler:           mux,
		ReadHeaderTimeout: time.Second * 1,
		WriteTimeout:      time.Second * 1,
		ReadTimeout:       time.Second * 1,
		IdleTimeout:       time.Second * 1,
	}

	if err := h.ListenAndServe(); err != nil {
		log.Println(err)
	}

}
