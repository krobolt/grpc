package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	pem "encoding/pem"
	"io/ioutil"
	"log"
	"net"
	"os"
	"time"

	pb "grpc-test/pb"
	users "grpc-test/users"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	port = ":50051"
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	pb.UnimplementedSyncServer
	users.UnimplementedUsersServer
}

type userserver struct {
	users.UnimplementedUsersServer
}

func (us *userserver) List(ctx context.Context, in *users.UsersRequest) (*users.UsersReply, error) {

	userCat := in.Category
	log.Printf("Received: %v", userCat)

	return &users.UsersReply{
		Id: []string{"dave", "bob", "andrew"},
	}, nil
}

// SayHello implements helloworld.GreeterServer
func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {

	name := in.GetName()
	log.Printf("Received: %v", name)
	return &pb.HelloReply{Message: "Hello " + name}, nil
}

func write(name string, pem []byte) error {
	f, err := os.Create(name + ".pem")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	f.Write(pem)
	return err
}

func gen() {
	_, priv, certpem, err := CreateRootCert([]string{"org"}, time.Hour*24, []net.IP{net.ParseIP("127.0.0.1")})
	if err != nil {
		panic(err)
	}
	//extract privkey
	pemblock, _ := pem.Decode(priv)
	private, err := x509.ParsePKCS1PrivateKey(pemblock.Bytes)
	if err != nil {
		log.Panicln(err)
	}
	//extract cert
	pemblock, _ = pem.Decode(certpem)
	rootcert, err := x509.ParseCertificate(pemblock.Bytes)
	if err != nil {
		log.Panicln(err)
	}

	write("key/rootkey", priv)
	write("key/rootcert", certpem)

	ips := []net.IP{net.ParseIP("127.0.0.1")}
	servKeyPEM, servCertPEM, err := CreateServerCert(rootcert, private, ips)
	if err != nil {
		log.Panicln(err)
	}

	write("key/serverkey", servKeyPEM)
	write("key/servercert", servCertPEM)

	pemblock, _ = pem.Decode(servKeyPEM)

	pemblock, _ = pem.Decode(servCertPEM)

	_, clientPEM, clientCertPEM, err := CreateClientCert(rootcert, private)
	if err != nil {
		log.Panicln(err)
	}

	write("key/clientkey", clientPEM)
	write("key/clientcert", clientCertPEM)

	panic(clientPEM)
	panic(clientCertPEM)
}

func ts() credentials.TransportCredentials {
	f, _ := os.Open("key/rootcert.pem")
	rootCertPEM, _ := ioutil.ReadAll(f)
	f.Close()

	certPool := x509.NewCertPool()
	certPool.AppendCertsFromPEM(rootCertPEM)

	f, _ = os.Open("key/servercert.pem")
	servCertPEM, _ := ioutil.ReadAll(f)
	f.Close()

	f, _ = os.Open("key/serverkey.pem")
	servKeyPEM, _ := ioutil.ReadAll(f)
	f.Close()

	servTLSCert, err := tls.X509KeyPair(servCertPEM, servKeyPEM)
	if err != nil {
		log.Fatalf("invalid key pair: %v", err)
	}

	tlsconfig := &tls.Config{
		InsecureSkipVerify: false,
		Certificates:       []tls.Certificate{servTLSCert},
		ClientAuth:         tls.RequireAndVerifyClientCert,
		ClientCAs:          certPool,
	}
	return credentials.NewTLS(tlsconfig)
}

func main() {

	creds := ts()

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer(
		grpc.Creds(creds),
	)

	pb.RegisterSyncServer(s, &server{})
	users.RegisterUsersServer(s, &userserver{})

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
