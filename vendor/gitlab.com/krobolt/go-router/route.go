package router

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"regexp"
	"strings"
)

//Route interface defines only basic root information
//ID: unique ident for url
//Alias: Additional possible routes (e.g. optional fields) are given alias with the same id as the parent route.
type Route interface {
	GetID() string
	GetMethod() string
	GetPath() string
	GetBase() string
	GetParams() []RouteParam
	GetAliases() []RouteAlias
}

type route struct {
	i      string
	m      string
	p      string
	params []RouteParam
	alias  []RouteAlias
}

//NewRoute helper function
func NewRoute(method string, path string, p []RouteParam, a []RouteAlias, id string) Route {
	return &route{
		m:      method,
		p:      path,
		i:      id,
		params: p,
		alias:  a,
	}
}

//GetID returns Route ident string
func (r *route) GetID() string {
	return r.i
}

//GetMethod return Route method string
func (r *route) GetMethod() string {
	return r.m
}

//GetPath returns path string
func (r *route) GetPath() string {
	return r.p
}

func (r *route) GetBase() string {
	for _, p := range r.params {
		if p.GetGroup() == "base" {
			return p.GetPath()
		}
	}
	return ""
}

//GetAliases returns routeAlias collection
func (r *route) GetParams() []RouteParam {
	return r.params
}

//GetAliases returns routeAlias collection
func (r *route) GetAliases() []RouteAlias {
	return r.alias
}

// CreateID creates an ident string from provided method and path
func CreateID(method string, path string) (string, error) {
	return _hashString(method + path)
}

func _hashString(s string) (string, error) {
	hasher := md5.New()
	hasher.Write([]byte(s))
	id := hex.EncodeToString(hasher.Sum(nil))
	if id == "" {
		return "", errors.New("Unable to create route id for route: " + s)
	}
	return id, nil
}

//CreateParamFromString returns Param from provided arguments
func CreateParamFromString(varSearch string, separator string, group string, s string, index int) (RouteParam, error) {

	//find partional links
	//todo: remove hard encoding !
	parts := strings.Split(s, "!")
	partial := false
	parturi := ""
	prefix := true

	if len(parts) > 1 {
		partial = true
		vs := strings.Split(parts[0], ":")
		if len(vs) > 1 {
			prefix = false
			parturi = parts[1]
		} else {
			prefix = true
			parturi = parts[0]
		}
	}

	rgx := regexp.MustCompile(varSearch)
	res := rgx.FindStringSubmatch(s)

	switch len(res) {
	case 0:
		checks := strings.Split(s, separator)
		if len(checks) < 1 {
			return _newErrorParam("invalid route part : " + s)
		}
		for _, c := range checks {
			if c == "" {
				return _newErrorParam("RouteParam can't be empty between seperators")
			}
		}
		return NewParam(group, s, s, s, "", index), nil
	case 3:
		regex := "[" + res[1] + "]+"
		if partial {
			if prefix {
				regex = "(" + parturi + ")" + "(" + regex + ")"
			} else {
				regex = "(" + regex + ")" + "(" + parturi + ")"
			}
		}
		psw := NewParam(group, s, regex, res[2], res[2], index)
		psw.SetPartional(parturi)
		return psw, nil

	default:
		return _newErrorParam("invalid route")
	}
}
